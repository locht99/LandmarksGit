//
//  LandmarksApp.swift
//  Landmarks
//
//  Created by Mr Du on 29/12/2022.
//

import SwiftUI

@main
struct LandmarksApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
